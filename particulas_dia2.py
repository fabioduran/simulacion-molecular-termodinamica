# Bibliotecas
from graphics import *
import time, random, math

# Parametros
num = 20
lx = 400.0; ly = 400.0; K = 2.0
dt = 0.1; pasos = 100000
R = 20.0; masa = 20.0
lj_e = 0.5; lj_r = 2.0*R
lj_r6 = lj_r**6; lj_r12 = lj_r**12

# Inicia la ventana de graficos
ventana = GraphWin("Simulacion", lx, ly)

x = []; x_prev = []; x_prox = []; fx = []
y = []; y_prev = []; y_prox = []; fy = []
for i in range(num):
    x.append(R + 2.5*(i%5)*R)
    y.append(R + 2.5*(i//5)*R)
    x_prev.append(x[i] - random.gauss(0,3)*dt)
    y_prev.append(y[i] - random.gauss(0,3)*dt)
    x_prox.append(0.0)
    y_prox.append(0.0)
    fx.append(0.0)
    fy.append(0.0)
    
# Dibuja la condicion inicial
ventana.autoflush = False
particula = []
for i in range(num):
    p = Circle(Point(x[i],y[i]),R)
    p.setFill('blue')
    p.draw(ventana)
    particula.append(p)
    
ventana.flush()
ventana.getMouse()

# Loop de pasos
for paso in range(pasos):
    # La energia potencial y la energia cinetica
    pe = 0.0; ke = 0.0
    # Las fuerzas suman de cero
    for i in range(num): fx[i]=0.0; fy[i]=0.0
    
    # Fuerza de los muros
    for i in range(num):
        if x[i] - R < 0:
            fx[i] += -K*(x[i]-R)
            pe += 0.5*K*(x[i]-R)**2
        if x[i] + R > lx:
            fx[i] += -K*(x[i]+R-lx)
            pe += 0.5*K*(x[i]+R-lx)**2
        if y[i] - R < 0:
            fy[i] += -K*(y[i]-R)
            pe += 0.5*K*(y[i]-R)**2
        if y[i] + R > ly:
            fy[i] += -K*(y[i]+R-ly)
            pe += 0.5*K*(y[i]+R-ly)**2

    # Interacciones entre particulas
    for i in range(num):
        for j in range(i+1,num):
            dx = x[j] - x[i]
            dy = y[j] - y[i]
            dist2 = dx**2 + dy**2
            iz = -12.0*lj_e*(lj_r12/dist2**7 - lj_r6/dist2**4)
            pe += lj_e*(lj_r12/dist2**6 - 2.0*lj_r6/dist2**3)
            fx[i] += iz*dx
            fy[i] += iz*dy
            fx[j] -= iz*dx
            fy[j] -= iz*dy
            
    # Integracion Verlet (La segunda ley de Newton)
    for i in range(num):
        x_prox[i] = 2*x[i] - x_prev[i] + fx[i]/masa*dt*dt
        y_prox[i] = 2*y[i] - y_prev[i] + fy[i]/masa*dt*dt

    # Calcula la energia cinetica
    for i in range(num):
        velx = 0.5*(x_prox[i]-x_prev[i])/dt
        vely = 0.5*(y_prox[i]-y_prev[i])/dt
        ke += 0.5*masa*(velx**2 + vely**2)
    print('Energy: '+str(ke+pe))
    
    # Cicla los variables
    for i in range(num):
        x_prev[i] = x[i]
        y_prev[i] = y[i]
        x[i] = x_prox[i]
        y[i] = y_prox[i]
    
    # Dibuja las particulas cada diez pasos
    if paso % 10 == 0:
        for i in range(num):
            particula[i].p1 = Point(x[i]-R, y[i]-R)
            particula[i].p2 = Point(x[i]+R, y[i]+R)
            particula[i].undraw()
            particula[i].draw(ventana)
        # Refresca los graficos
        time.sleep(0.005)
        ventana.flush()

