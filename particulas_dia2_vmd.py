# Bibliotecas
import time, random, math

# Parametros
num = 30
pasos = 10000
timestep = 2.0; # en femtosegundos
lx = 40.0; # en angstroms 
ly = 40.0; # en angstroms 
K = 20.0; # en kcal/mol/angstrom^2
masa = 20.017970; # en daltons
lj_e = 0.08545; # en kcal/mol
lj_r = 3.06; # en angstroms
R = 0.5*lj_r; # en angstroms
archivo_salida = 'particulas_dia2.pdb'

lj_r6 = lj_r**6; lj_r12 = lj_r**12

def escribe_frame(salida,x,y):
    name = 'NE'
    resname = 'NE'
    chain = 'A'
    segname = 'A'
    for i in range(len(x)):
        resid = i + 1
        salida.write('ATOM  %5d  %-3s %-4s%s%4d    %8.3f%8.3f%8.3f %5.2f %5.2f      %-4s\n' % (i+1, name, resname, chain, resid, x[i], y[i], 0.0, 1.0, 1.0, segname))
    salida.write('END\n')

factor_tiempo = 48.88821
dt = timestep/factor_tiempo
escala = 10.0

x = []; x_prev = []; x_prox = []; fx = []
y = []; y_prev = []; y_prox = []; fy = []
for i in range(num):
    x.append(R + 2.5*(i%5)*R)
    y.append(R + 2.5*(i//5)*R)
    x_prev.append(x[i] - random.gauss(0,0.1)*dt)
    y_prev.append(y[i] - random.gauss(0,0.1)*dt)
    x_prox.append(0.0)
    y_prox.append(0.0)
    fx.append(0.0)
    fy.append(0.0)

# Abre el archivo
salida = open(archivo_salida, 'w')
escribe_frame(salida,x,y)
    
# Loop de pasos
for paso in range(pasos):
    # La energia potencial y la energia cinetica
    pe = 0.0; ke = 0.0
    # Las fuerzas suman de cero
    for i in range(num): fx[i]=0.0; fy[i]=0.0
    
    # Fuerza de los muros
    for i in range(num):
        if x[i] - R < 0:
            fx[i] += -K*(x[i]-R)
            pe += 0.5*K*(x[i]-R)**2
        if x[i] + R > lx:
            fx[i] += -K*(x[i]+R-lx)
            pe += 0.5*K*(x[i]+R-lx)**2
        if y[i] - R < 0:
            fy[i] += -K*(y[i]-R)
            pe += 0.5*K*(y[i]-R)**2
        if y[i] + R > ly:
            fy[i] += -K*(y[i]+R-ly)
            pe += 0.5*K*(y[i]+R-ly)**2

    # Interacciones entre particulas
    for i in range(num):
        for j in range(i+1,num):
            dx = x[j] - x[i]
            dy = y[j] - y[i]
            dist2 = dx**2 + dy**2
            iz = -12.0*lj_e*(lj_r12/dist2**7 - lj_r6/dist2**4)
            pe += lj_e*(lj_r12/dist2**6 - 2.0*lj_r6/dist2**3)
            fx[i] += iz*dx
            fy[i] += iz*dy
            fx[j] -= iz*dx
            fy[j] -= iz*dy
            
    # Integracion Verlet (La segunda ley de Newton)
    for i in range(num):
        x_prox[i] = 2*x[i] - x_prev[i] + fx[i]/masa*dt*dt
        y_prox[i] = 2*y[i] - y_prev[i] + fy[i]/masa*dt*dt

    # Calcula la energia cinetica
    for i in range(num):
        velx = 0.5*(x_prox[i]-x_prev[i])/dt
        vely = 0.5*(y_prox[i]-y_prev[i])/dt
        ke += 0.5*masa*(velx**2 + vely**2)
    print('Energy: '+str(ke+pe))
    
    # Cicla los variables
    for i in range(num):
        x_prev[i] = x[i]
        y_prev[i] = y[i]
        x[i] = x_prox[i]
        y[i] = y_prox[i]
    
    # Escribe el frame
    if paso % 10 == 0:
        escribe_frame(salida,x,y)

salida.close()
