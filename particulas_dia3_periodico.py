# Bibliotecas
import time, random, math, sys

num = int(sys.argv[1])

# Parametros
pasos = 10000
timestep = 2.0; # en femtosegundos
lx = 40.0; # en angstroms 
ly = 40.0; # en angstroms 
K = 20.0; # en kcal/mol/angstrom^2
masa = 20.017970; # en daltons
lj_e = 0.08545; # en kcal/mol
lj_r = 3.06; # en angstroms
R = 0.5*lj_r; # en angstroms
archivo_salida = 'particulas_dia3_periodico.pdb'
cutoff = 5.0; # en angstroms

lj_r6 = lj_r**6; lj_r12 = lj_r**12

def plega(d,l):
    imagen = math.floor(d/l)
    d -= imagen*l
    if d >= 0.5*l:
        d -= l
    return d

def escribe_frame(salida,x,y):
    name = 'NE'
    resname = 'NE'
    chain = 'A'
    segname = 'A'
    # once espacios
    salida.write('CRYST1 %8.3f %8.3f %8.3f  90.00  90.00  90.00 P 1           1\n' % (lx, ly, 10.0))
    for i in range(len(x)):
        resid = i + 1
        xp = plega(x[i], lx)
        yp = plega(y[i], ly)
        salida.write('ATOM  %5d  %-3s %-4s%s%4d    %8.3f%8.3f%8.3f %5.2f %5.2f      %-4s\n' % (i+1, name, resname, chain, resid, xp, yp, 0.0, 1.0, 1.0, segname))
    salida.write('END\n')
    
def get_vecinos(x0,y0):
    nx = int(math.floor(lx/cutoff))
    ny = int(math.floor(ly/cutoff))
    tamano_celda_x = lx/nx
    tamano_celda_y = lx/ny
    
    ix = int(math.floor(x0/tamano_celda_x))
    iy = int(math.floor(y0/tamano_celda_y))
    ret = []
    for jx in range(ix-1,ix+2):
        for jy in range(iy-1,iy+2):
            jx1 = int(plega(jx,nx))
            jy1 = int(plega(jy,ny))
            c1 = jx1 + nx*jy1
            ret.append(c1)
    return ret
    
def descomposicion(x,y):
    celda = {}
    for i in range(num):
        vecinos = get_vecinos(x[i], y[i])
        c = vecinos[4]
        if c in celda.keys():
            celda[c].append(i)
        else:
            celda[c] = [i]
    return celda
    
    
factor_tiempo = 48.88821
dt = timestep/factor_tiempo
escala = 10.0

x = []; x_prev = []; x_prox = []; fx = []
y = []; y_prev = []; y_prox = []; fy = []
for i in range(num):
    x.append(R + 2.5*(i%5)*R)
    y.append(R + 2.5*(i//5)*R)
    x_prev.append(x[i] - random.gauss(0,0.1)*dt)
    y_prev.append(y[i] - random.gauss(0,0.1)*dt)
    x_prox.append(0.0)
    y_prox.append(0.0)
    fx.append(0.0)
    fy.append(0.0)

# Abre el archivo
salida = open(archivo_salida, 'w')
escribe_frame(salida,x,y)
    
tiempo_empiezo = time.time()
# Loop de pasos
for paso in range(pasos):
    # La energia potencial y la energia cinetica
    pe = 0.0; ke = 0.0
    # Las fuerzas suman de cero
    for i in range(num): fx[i]=0.0; fy[i]=0.0
    
    # # Fuerza de los muros
    # for i in range(num):
    #     if x[i] - R < 0:
    #         fx[i] += -K*(x[i]-R)
    #         pe += 0.5*K*(x[i]-R)**2
    #     if x[i] + R > lx:
    #         fx[i] += -K*(x[i]+R-lx)
    #         pe += 0.5*K*(x[i]+R-lx)**2
    #     if y[i] - R < 0:
    #         fy[i] += -K*(y[i]-R)
    #         pe += 0.5*K*(y[i]-R)**2
    #     if y[i] + R > ly:
    #         fy[i] += -K*(y[i]+R-ly)
    #         pe += 0.5*K*(y[i]+R-ly)**2

    # Cell decomposition
    celda = descomposicion(x,y)

    # Interacciones entre particulas
    for i in range(num):
        vecinos = get_vecinos(x[i],y[i])

        parejas = []
        for c in vecinos:
            if c in celda:
                for k in celda[c]:
                    if k != i:
                        parejas.append(k)

        for j in parejas:
            dx = plega(x[j] - x[i], lx)
            dy = plega(y[j] - y[i], ly)
            dist2 = dx**2 + dy**2
            if dist2 < cutoff**2:
                iz = -12.0*lj_e*(lj_r12/dist2**7 - lj_r6/dist2**4)
                pe += 0.5*lj_e*(lj_r12/dist2**6 - 2.0*lj_r6/dist2**3)
                fx[i] += iz*dx
                fy[i] += iz*dy
            
    # Integracion Verlet (La segunda ley de Newton)
    for i in range(num):
        x_prox[i] = 2*x[i] - x_prev[i] + fx[i]/masa*dt*dt
        y_prox[i] = 2*y[i] - y_prev[i] + fy[i]/masa*dt*dt

    # Calcula la energia cinetica
    for i in range(num):
        velx = 0.5*(x_prox[i]-x_prev[i])/dt
        vely = 0.5*(y_prox[i]-y_prev[i])/dt
        ke += 0.5*masa*(velx**2 + vely**2)
    
    # Cicla los variables
    for i in range(num):
        x_prev[i] = x[i]
        y_prev[i] = y[i]
        x[i] = x_prox[i]
        y[i] = y_prox[i]
    
    # Escribe el frame
    if paso % 100 == 0:
        escribe_frame(salida,x,y)
        print('Energia: '+str(ke+pe))

tiempo_final = time.time()
print('Tiempo: ' + str(tiempo_final - tiempo_empiezo))
salida.close()
