# Bibliotecas
import time
import random
import math


# Parametros
num = 40
archivo_salida = 'oxigeno_pairlist.pdb'
pasos = 50000
timestep = 1.0; # en femtosegundos
lx = 40.0; # en angstroms 
ly = 40.0; # en angstroms 
masa = 15.99900; # en daltons
lj_e = 0.12; # en kcal/mol
lj_r = 3.40; # en angstroms
R = 0.5*lj_r; # en angstroms
bond_k = 600.000; # kcal/mol/angstrom^2
bond_r = 1.2300; # angstrom
cutoff = 8.0; # en angstroms
pairlist_dist = 9.5; # en angstroms
pairlist_pasos = 10; # pasos
vel_i = 0.002; # en angstroms/fs

# Valores
factor_tiempo = 48.88821; # convierte desde fs a 
dt = timestep/factor_tiempo; # en sqrt(kcal/mol/Da)
vel_i1 = 0.002*factor_tiempo

def plega(d,l):
    imagen = math.floor(d/l)
    d -= imagen*l
    if d >= 0.5*l:
        d -= l
    return d

def escribe_frame(salida,x,y):
    name = 'O2'
    resname = 'O2'
    chain = 'A'
    segname = 'A'
    # once espacios
    salida.write('CRYST1 %8.3f %8.3f %8.3f  90.00  90.00  90.00 P 1           1\n' % (lx, ly, 10.0))
    for i in range(len(x)):
        resid = i + 1
        xp = plega(x[i], lx)
        yp = plega(y[i], ly)
        salida.write('ATOM  %5d  %-3s %-4s%s%4d    %8.3f%8.3f%8.3f %5.2f %5.2f      %-4s\n' % (i+1, name, resname, chain, resid, xp, yp, 0.0, 1.0, 1.0, segname))
    salida.write('END\n')

class LennardJonesCalculator:
    # Construct a Lennard Jones force computer
    def __init__(self, lj_e, lj_r, cutoff):
        self.lj_e = lj_e
        self.lj_r = lj_r
        self.cutoff = cutoff
        self.lj_r6 = lj_r**6
        self.lj_r12 = lj_r**12
        self.cutoff2 = cutoff**2
  
    def calc_force(self, dx, dy):
        dist2 = dx**2 + dy**2
        if dist2 >= self.cutoff2:
            return [0.0, 0.0]
        else:
            dist8 = dist2**4
            dist14 = dist8*dist2**3
            factor = -12.0*self.lj_e*(self.lj_r12/dist14 - self.lj_r6/dist8)
            return [factor*dx, factor*dy]

    def calc_energy(self, dx, dy):
        dist2 = dx**2 + dy**2
        if dist2 >= self.cutoff2:
            return 0.0
        else:
            dist6 = dist2**3
            dist12 = dist6**2
            return self.lj_e*(self.lj_r12/dist12 - 2.0*self.lj_r6/dist6)
    
class CellDecomposition:
    # Construct a cell decomposition
    def __init__(self,lx,ly,pairlist_dist):
        # Geometry of cell decomposition
        self.lx = lx
        self.ly = ly
        self.nx = int(math.floor(lx/pairlist_dist))
        self.ny = int(math.floor(ly/pairlist_dist))
        self.cell_lx = lx/self.nx
        self.cell_ly = ly/self.ny
        self.num_cells = self.nx*self.ny
        self.pairlist_dist2 = pairlist_dist*pairlist_dist

        # Make list of neighbors for each cell
        self.cell_neigh = []
        for c in range(self.num_cells):
            self.cell_neigh.append(self._get_neighbors(c))

    # Wrap a difference on periodic boundaries so that -l/2 <= d < l/2
    def wrap_delta(self,d,l):
        imagen = math.floor(d/l)
        d -= imagen*l
        if d >= 0.5*l:
            d -= l
        return d
            
    # Convert spatial positions to a 1D cell index
    def pos_to_cell(self,x0,y0):
        # Deal with periodic boundary conditions
        ix = int(math.floor(x0/self.cell_lx)) % self.nx
        iy = int(math.floor(y0/self.cell_ly)) % self.ny
        return self.cell_index(ix,iy)
            
    # Convert 2D cell indices to a 1D cell index
    def cell_index(self,ix,iy):
        return ix + self.nx*iy

    # Convert 1D cell indices to 2D cell indices
    def cell_index_2D(self,c):
        return [c%self.nx, c//self.nx]

    # Get the neighbors for a cell c
    def _get_neighbors(self,c):
        [ix, iy] = self.cell_index_2D(c)
        neigh_list = []
        for jx in range(ix-1,ix+2):
            for jy in range(iy-1,iy+2):
                # Periodic boundary conditions
                jx1 = jx % self.nx
                jy1 = jy % self.ny
                neigh_list.append(self.cell_index(jx1,jy1))
        return neigh_list
    
    # Return the particle indices in each cell
    def decompose(self,x_list,y_list):
        # Clear the cells.
        cell_particles = []
        for c in range(self.num_cells):
            cell_particles.append([])

        for i in range(len(x_list)):
            c = self.pos_to_cell(x_list[i], y_list[i])
            cell_particles[c].append(i)
        return cell_particles

    # For each particle, make a list of particles within pairlist_dist
    def make_pairlists(self,x_list,y_list):
         cell_particles = self.decompose(x_list,y_list)
         pairlist = []
         for i in range(len(x_list)):
             cell = self.pos_to_cell(x_list[i],y_list[i])
             # Check distance of particles in the neighboring cells
             pairlist.append([])
             for neigh in self.cell_neigh[cell]:
                 for j in cell_particles[neigh]:
                     if i != j:
                         dx = self.wrap_delta(x_list[j]-x_list[i], self.lx)
                         dy = self.wrap_delta(y_list[j]-y_list[i], self.ly)
                         dist2 = dx*dx + dy*dy
                         if dist2 < self.pairlist_dist2:
                             pairlist[i].append(j)
         return pairlist


# Condiciones iniciales
x = [];
y = [];
x0 = -0.5*lx + R
y0 = -0.5*ly + R
for i in range(0,num//2):
    x.append(x0)
    y.append(y0)
    x.append(x0 + int(100.0*0.05*math.cos(i))*0.01)
    y.append(y0 + 1.2)
    x0 += 2.5*R
    if (x0 > 0.5*lx - 2.5*R):
        x0 = -0.5*lx + R
        y0 += 2.5*R


# Prepara la descomposicion de celdas
decomp = CellDecomposition(lx,ly,pairlist_dist)
pairlist = decomp.make_pairlists(x,y)

# Prepara el objeto para calcular la energia Lennard-Jones
lj = LennardJonesCalculator(lj_e, lj_r, cutoff)

# Agrega enlaces a pares para oxigeno
enlaces = []
for i in range(0,num,2):
    enlaces.append([i, i+1])

x_prev = []; x_prox = []; fx = []
y_prev = []; y_prox = []; fy = []
for i in range(num):
    # Velocidades iniciales
    x_prev.append(x[i] - random.gauss(0,1)*vel_i1*dt)
    y_prev.append(y[i] - random.gauss(0,1)*vel_i1*dt)
    # Prepara los otros arrays
    x_prox.append(0.0)
    y_prox.append(0.0)
    fx.append(0.0)
    fy.append(0.0)

# Abre el archivo
salida = open(archivo_salida, 'w')
escribe_frame(salida,x,y)
    
tiempo_empiezo = time.time()
# Loop de pasos
for paso in range(pasos):
    # La energia potencial y la energia cinetica
    pe = 0.0
    ke = 0.0
    # Las fuerzas empiezan a cero
    for i in range(num):
        fx[i]=0.0
        fy[i]=0.0

    # Fuerzas de enlaces covalentes
    for b in enlaces:
        i = b[0]
        j = b[1]
        dx = x[j] - x[i]
        dy = y[j] - y[i]
        dist = math.sqrt(dx**2 + dy**2)
        fuerza = 2.0*bond_k*(dist - bond_r)
        fx[i] += fuerza*dx/dist
        fy[i] += fuerza*dy/dist
        fx[i+1] -= fuerza*dx/dist
        fy[i+1] -= fuerza*dy/dist
        pe += bond_k*(dist - bond_r)**2
    
    # Interacciones entre particulas usando la lista de pares
    for i in range(num):
        parejas = pairlist[i]

        for j in parejas:
            # No hay interacciones entre particulas con enlaces covalentes
            if ([i, j] in enlaces) or ([j, i] in enlaces):
                continue
            
            dx = plega(x[j] - x[i], lx)
            dy = plega(y[j] - y[i], ly)
            pe += 0.5*lj.calc_energy(dx, dy)
            fuerza = lj.calc_force(dx, dy)
            fx[i] += fuerza[0]
            fy[i] += fuerza[1]
            
    # Integracion Verlet (La segunda ley de Newton)
    for i in range(num):
        x_prox[i] = 2*x[i] - x_prev[i] + fx[i]/masa*dt*dt
        y_prox[i] = 2*y[i] - y_prev[i] + fy[i]/masa*dt*dt

    # Calcula la energia cinetica
    for i in range(num):
        velx = 0.5*(x_prox[i]-x_prev[i])/dt
        vely = 0.5*(y_prox[i]-y_prev[i])/dt
        ke += 0.5*masa*(velx**2 + vely**2)
    
    # Cicla los variables
    for i in range(num):
        x_prev[i] = x[i]
        y_prev[i] = y[i]
        x[i] = x_prox[i]
        y[i] = y_prox[i]

    # Actualiza los pairlist
    if paso % pairlist_pasos == 0:
        pairlist = decomp.make_pairlists(x,y)
        
    # Escribe el frame
    if paso % 100 == 0:
        escribe_frame(salida,x,y)
        print('Potential: '+ str(pe) + ' Total: ' + str(ke+pe))

tiempo_final = time.time()
print('Tiempo: ' + str(tiempo_final - tiempo_empiezo))
salida.close()
